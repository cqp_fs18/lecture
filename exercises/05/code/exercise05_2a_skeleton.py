from projectq.ops import Rx, Rz, CNOT, Measure
from projectq.meta import Compute, Uncompute
from projectq import MainEngine


# time evolution under the transverse field Ising Hamiltonian
# H = J * sum_i sigma^i_z sigma^{i+1}_z - Gamma * sum_i sigma^i_x
def tfim_time_evolution(wavefunction, parameters):
    # read in parameters
    eng = wavefunction[0].engine # get engine from the first qubit
    num_trotter_slices = parameters['num_trotter_slices']
    num_sites = len(wavefunction)
    dt = parameters['time'] / num_trotter_slices
    coupling_strength = parameters['coupling_strength']
    transverse_field = parameters['transverse_field']

    # apply Trotterized time evolution
    for _ in range(num_trotter_slices):
        # apply Ising term
        for s1 in range(num_sites):
            s2 = (s1 + 1) % num_sites
            # TODO implement

        # apply transverse-field term
        for s in range(num_sites):
            # TODO implement


if __name__ == "__main__":
    # Initialize evolution parameters
    parameters = {
                    'time': 1.,
                    'num_trotter_slices': 100,
                    'coupling_strength': 1.,
                    'transverse_field': 1.
                 }
    # number of spin sites
    num_sites = 10

    # initialize the main compiler engine and allocate the wavefunction
    eng = MainEngine()
    wavefunction = eng.allocate_qureg(num_sites)
    # apply the time evolution
    tfim_time_evolution(wavefunction, parameters)

    # measure the wavefunction and output the outcome
    Measure | wavefunction
    eng.flush()
