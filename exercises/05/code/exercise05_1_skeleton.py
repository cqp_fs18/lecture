from projectq import MainEngine
from projectq.ops import H, X, CNOT, Rz, Measure


"""
Runs Deutsch's algorithm for a given oracle.
The oracle is a function which accepts two qubits (input and output)
and applies the function to the output qubit.
"""
def deutsch(oracle):
    eng = MainEngine()
    qubit_in = eng.allocate_qubit()
    qubit_out = eng.allocate_qubit()

    # TODO initialize the two qubits

    # apply the oracle
    oracle(qubit_in, qubit_out)

    # TODO add something here

    # measure the qubits and output whether the function
    # is constant or balanced
    Measure | (qubit_in, qubit_out)
    result = int(qubit_in)
    return ['constant','balanced'][result]


"""
oracle1 applies a controlled not, i.e., it is balanced:
0,0 --> 0,0
1,0 --> 1,1
"""
def oracle1(qubit_in, qubit_out):
    CNOT | (qubit_in, qubit_out)


"""
oracle2 is the identity, i.e., it is constant:
0,0 --> 0,0
1,0 --> 1,0
"""
def oracle2(qubit_in, qubit_out):
    pass


if __name__ == "__main__":
    print("Running Deutsch's algorithm for two oracles.")
    print("Oracle 1 is {}".format(deutsch(oracle1)))
    print("Oracle 2 is {}".format(deutsch(oracle2)))
