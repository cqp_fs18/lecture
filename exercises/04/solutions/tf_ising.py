#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# CQP FS18
# Transverse field Ising model: solution code

from __future__ import division, print_function, unicode_literals

import copy
import random
from cmath import exp, sin, cos

import numpy as np
import scipy.linalg as la
import matplotlib
import matplotlib.pyplot as plt


def evolve(l, j, hx, v_start, dt, n):
    """ Evolve the state v in time by n * dt

    Parameters:
    l       length of chain
    j       Function returning the Ising coupling for each neighbors, given time t.
            The Ising coupling is a 1d numpy array with length l-1 (non-periodic)
    hx      Function returning the transverse field for each site, given time t.
            The transverse field is a 1d numpy array with length l
    v_start State at time t, complex 1d numpy array with length 2**l
    dt      timestep
    n       number of consecutive time steps

    Return:
    v  the state at time t+n*dt
    """
    dim = 2**l
    v = np.array(v_start, dtype=complex)

    def tstep_diag(x, t, dt):
        # Apply diagonal part
        j_now = j(t)
        for s in range(dim):
            jtotal = 0.
            breaks = s ^ (s >> 1)
            for r in range(l - 1):
                jtotal += j_now[r] if breaks & (1 << r) else -j_now[r]
            x[s] = exp(-1j * jtotal * dt) * x[s]
        return x

    # Part A
    def tstep_transv(x, t, dt):
        # Apply transverse part succesively for each site
        y = np.zeros_like(x)
        hx_now = hx(t)
        for r in range(l):
            # start from the right
            cos_v = cos(dt * hx_now[r])
            sin_v = sin(dt * hx_now[r])
            # diagonal
            for s in range(dim):
                y[s] = cos_v * x[s]
            # off-diagonal
            for s in range(dim):
                y[s ^ (1 << r)] += 1.j * sin_v * x[s]
            # swap vectors
            x, y = y, x
        return x
    v = tstep_diag(v, t=0, dt=dt / 2.)
    for i in range(int(n - 1)):
        v = tstep_transv(v, t=i * dt, dt=dt)
        v = tstep_diag(v, t=(i + 0.5) * dt, dt=dt)
        v /= la.norm(v)
    v = tstep_transv(v, t=(n - 1) * dt, dt=dt)
    v = tstep_diag(v, t=(n - 0.5) * dt, dt=dt / 2.)
    v /= la.norm(v)
    return v

def magnetization(v, l):
    """ Measure the magnetization per site for the state v

    Parameters:
    v  the state to be measured, complex 1d numpy array with length 2**l
    l  length of chain

    Return:
    m  the magnetization at each site, 1d numpy array with length l
    """
    dim = 2**l
    m = np.zeros(l)
    for r in range(l):
        for s in range(dim):
            m[r] += abs(v[s])**2 * 2. * (bool(s & (1 << r)) - 0.5)
    return m

def evolve_static(J, hx, v_start, t_end=20, t_meas=0.5, dt=0.1):
    """
    Evolve the state with fixed Ising terms and transverse field.

    Parameters:
        J       (scalar, or np.array of length l - 1) Ising parameters. If the parameter is scalar, the same parameter will be used for all J_i
        hx      (scalar, or np.array of length l) Transverse field strength.
        v_start State at time t=0, complex 1d numpy array with length 2**l
        t_end   time until which the state is evolved
        t_meas  time step between measurements
        dt      time step for evolution

    Returns:
        (states, times)
        where 'states' is a numpy array containing the state at each measurement step
        and 'times' is a list containing the time at each of these steps
    """
    dim = len(v_start)
    l = int(np.round(np.log2(dim)))
    # Here the parameters j and hx do not change with time.
    j_static = lambda t: J * np.ones(l - 1)
    hx_static = lambda t: hx * np.ones(l)

    # Parameters for time evolution
    nmeas = int((t_end) / t_meas)  # number of measurements
    nstep = int((t_meas) / abs(dt))  # number of steps between measurments

    # Do time evolution
    states = np.zeros((nmeas, dim), dtype=complex)

    states[0, :] = copy.deepcopy(v_start)
    for t in range(1, nmeas):
        print(t * t_meas)
        states[t, :] = evolve(l, j_static, hx_static, v_start=states[t - 1, :], dt=dt, n=nstep)

    times = [nstep * abs(dt) * i for i in range(nmeas)]
    return states, times

def anneal(J, v_start, dt, n):
    """
    Annealing scheme.

    Parameters:
        * J (np.array): Ising parameters
        * v_start (np.array): initial state
        * dt: time step
        * n: number of time steps

    Returns:
        * Final state (np.array) after annealing
    """
    l = len(J) + 1
    J_dynamic = lambda t: (t / (n * dt)) * np.array(J)
    hx_dynamic = lambda t: (1 - t / (n * dt)) * np.ones(l)
    return evolve(l=l, j=J_dynamic, hx=hx_dynamic, v_start=v_start, dt=dt, n=n)

def plot_magnetization(J, hx, v_start, plot_name):
    # Parameters for time evolution
    t_end = 20.  # duration of the time evolution
    t_meas = 0.5  # time after which a measurment is performed
    dt = 0.1  # time step used for the evolution

    states, times = evolve_static(J, hx, v_start, t_end=t_end, t_meas=t_meas, dt=dt)

    nmeas = len(states)
    dim = len(v_start)
    l = int(np.round(np.log2(dim)))

    # measure magnetization
    m = np.array([magnetization(v, l) for v in states])

    # plot magnetization
    plt.pcolor(np.array(range(l + 1)), np.array(times), np.array(m),
               vmin=-1.0, vmax=1.0)
    plt.xlabel('site')
    plt.ylabel('time')
    plt.xticks([x + 0.5 for x in range(l)], range(1, l + 1))
    plt.axis([0, l, 0, times[-1]])
    plt.title("Magnetization, $J_0$ = " + str(J) + " , $h^x_0$ = " + str(hx))
    plt.colorbar()
    plt.savefig(plot_name + '.pdf', bbox_inches='tight')
    plt.clf()

def plot_energy(J, states, times, ax):
    def get_energy(J, state):
        E_total = 0
        for s, v in enumerate(state):
            E_state = 0
            breaks = s ^ (s >> 1)
            for r, j in enumerate(J):
                E_state += j if breaks & (1<<r) else -j
            E_total += E_state * abs(v)**2
        return E_total

    groundstate_energy = -np.sum(np.abs(J))
    energies = [get_energy(J, s) for s in states]

    ax.plot(times, energies, label='$E$')
    ax.axhline(groundstate_energy, color=col_vec[1], label='$E_0$')
    ax.legend()
    ax.set_xlabel('$T$')
    ax.set_ylabel('$E$', rotation='horizontal')
    ax.set_xlim(0, max(times))
    ax.set_ylim(groundstate_energy - 1, max(energies) + 1)

def plot_order_parameter(states, times, ax, color, label):
    def get_order_parameter(state):
        M = 0
        dim = len(state)
        l = int(np.round(np.log2(dim)))
        for s, v in enumerate(state):
            M_state = 2 * abs(bin(s).count('1') - l / 2)
            M += M_state * abs(v)**2
        return M / l

    order_params = [get_order_parameter(s) for s in states]
    ax.plot(times, order_params, label=label, color=color)
    ax.set_xlabel('$T$')
    ax.set_ylabel(r'$\langle|M|\rangle$', rotation='horizontal', ha='right')
    ax.set_xlim(0, max(times))
    ax.set_ylim(0, 1.1)

if __name__ == '__main__':
    # Part B
    print('b)')
    l = 9
    ind = 1 << ((l - 1) // 2)
    dim = 2**l
    v = np.zeros(dim, dtype=complex)
    v[ind] = 1.

    plot_magnetization(J=0, hx=0.2, v_start=v, plot_name='magnetization_1')
    plot_magnetization(J=0, hx=0.5, v_start=v, plot_name='magnetization_2')
    plot_magnetization(J=1, hx=0.2, v_start=v, plot_name='magnetization_3')
    plot_magnetization(J=1, hx=0.5, v_start=v, plot_name='magnetization_4')

    # Part C
    print('c)')
    random.seed(42)
    J_random = [random.uniform(-1, 1) for _ in range(l - 1)]
    T = [1, 3, 5, 10, 15, 20, 40]
    dt = 0.1
    N = [int(t / dt) for t in T]
    v_start = np.ones(dim) / np.sqrt(dim)
    states = [anneal(J=J_random, v_start=v_start, dt=dt, n=n) for n in N]
    fig, ax = plt.subplots()
    plot_energy(J_random, states=states, times=T, ax=ax)
    plt.title('Final energy as a function of annealing time')
    plt.savefig('annealing_energies.pdf', bbox_inches='tight')

    # Part D
    print('d)')
    # Energy of the disordered system
    t_end = 10
    t_meas = 0.5
    dt = -0.1j
    states, times = evolve_static(J=J_random, hx=np.zeros(l), v_start=v_start, t_end=t_end, t_meas=t_meas, dt=dt)
    fig, ax = plt.subplots()
    plot_energy(J=J_random, states=states, times=times, ax=ax)
    plt.title('Energy as a function of imaginary time.')
    plt.savefig('im_time_disordered.pdf', bbox_inches='tight')

    # Ferromagnetic order parameter
    t_end = 2
    t_meas = 0.1
    dt = -0.02j
    fig, ax = plt.subplots()
    for hx, color in zip([0, 0.5, 1.], col_vec):
        states, times = evolve_static(J=1, hx=hx, v_start=v_start, t_end=t_end, t_meas=t_meas, dt=dt)
        plot_order_parameter(states, times, ax=ax, label='$h^x={}$'.format(hx), color=color)
    ax.legend()
    plt.title('Order parameter as a function of imaginary time, for different $h^x$')
    plt.savefig('im_time_field.pdf', bbox_inches='tight')
