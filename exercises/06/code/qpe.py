import math

from projectq.meta import Control
from projectq.ops import Measure, H, R


# peforms quantum phase estimation
# oracle: unitary to measure the phase of
# wavefunction: input wavefunction
# bits: number of bits to resolve
def QPE(eng, oracle, wavefunction, bits):
    m = [0] * bits # measurement results
    for b in range(bits):
        # allocate quantum phase estimation ancilla qubit
        qpe_qubit = eng.allocate_qubit()
        H | qpe_qubit
        # number of times to call the oracle
        num_evolve = 2 ** (bits - 1 - b)
        # perform controlled evolution
        with Control(eng, qpe_qubit):
            for _ in range(num_evolve):
                oracle(wavefunction)
        # perform inverse quantum Fourier transform
        for i in range(b):
            if m[i]:
                R(-math.pi/(1 << (b - i))) | qpe_qubit
        # final Hadamard gate
        H | qpe_qubit
        # and measure
        Measure | qpe_qubit
        eng.flush()
        # store the measurement result
        m[b] = int(qpe_qubit)
        del qpe_qubit
    frac = sum((m[i] << i) for i in range(bits)) / (2. ** bits)
    return frac * 2 * math.pi
