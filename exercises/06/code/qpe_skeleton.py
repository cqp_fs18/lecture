import math

from projectq.meta import Control
from projectq.ops import Measure, H, R


# peforms quantum phase estimation
# oracle: unitary to measure the phase of
# wavefunction: input wavefunction
# bits: number of bits to resolve
def QPE(eng, oracle, wavefunction, bits):
    m = [0] * bits # measurement results
    for b in range(bits):
        # allocate quantum phase estimation ancilla qubit
        qpe_qubit = eng.allocate_qubit()
        H | qpe_qubit
        # number of times to call the oracle
        num_evolve = # TODO: determine how many times the oracle should be called
        # run the oracle controlled on the ancilla qubit

        # TODO run oracle num_evolve times controlled on ancilla qubit

        # perform inverse quantum Fourier transform using previous
        # measurement results


        # TODO perform controlled R-gates and final Hadamard

        # and measure
        Measure | qpe_qubit
        eng.flush()
        # store the measurement result
        m[b] = int(qpe_qubit)
        del qpe_qubit
    frac = sum((m[i] << i) for i in range(bits)) / (2. ** bits)
    return frac * 2 * math.pi
