import math

from projectq.ops import QubitOperator, Measure, H, All, X
from projectq import MainEngine

from exercise05_2a import tfim_time_evolution
from qpe import QPE


if __name__ == "__main__":
    num_sites = 8 # an even number of sites (we have periodic boundary conditions)
    annealing_steps = 20 # number of annealing time steps

    # create the main compiler engine and allocate the wavefunction
    eng = MainEngine()
    wavefunction = eng.allocate_qureg(num_sites)

    # initialize to eigenstate of transverse field
    All(H) | wavefunction

    parameters = { 'time': 1., 'num_trotter_slices': 5 }
    # loop over number of annealing steps
    for t in range(annealing_steps):
        # update parameters for current annealing step
        parameters['coupling_strength'] = (t * 1. / annealing_steps)
        parameters['transverse_field'] = 1. - parameters['coupling_strength']
        # perform time evolution
        tfim_time_evolution(wavefunction, parameters)

    # perform quantum phase estimation on the final Hamiltonian:
    parameters['time'] = math.pi / num_sites
    parameters['num_trotter_slices'] = 5
    parameters['coupling_strength'] = 1.
    parameters['transverse_field'] = 0.
    # define the unitary which implements time evolution:
    def U(qubits):
        tfim_time_evolution(qubits, parameters)
    # and call quantum phase estimation
    phase = QPE(eng, U, wavefunction, 5)
    energy = -phase / parameters['time']
    print("Measured energy: {}".format(energy))
    # now measure and output the measurement result
    Measure | wavefunction
    eng.flush()
    results = [int(qubit) for qubit in wavefunction]
    print('Measurement result: {}'.format(results))
