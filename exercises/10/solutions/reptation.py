#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Computation Quantum Physics, FS18

from __future__ import division, print_function

import abc

import numpy as np

from initial_states import GaussianInitialState

class ReptationQMC:
    """
    This class implements the Reptation Quantum Monte Carlo for 1d particles.
    """
    def __init__(self, psi, potential, tau, dtau):

        # total imaginary time
        self.tau = tau

        # number of time slices
        self.P = int(tau / dtau)
        assert self.P >= 1

        # we recompute the time step to avoid truncation problems in the
        # division
        self.Dt = tau / float(self.P)
        self.SqrDt = np.sqrt(self.Dt)

        # path of configurations
        self.confs = np.zeros((2 * self.P + 1))

        # setting the wave-function and the potential
        self.psi = psi
        self.potential = potential

        # Initializing the path configurations with free propagation
        for i in range(1, len(self.confs)):
            self.confs[i] = self.get_new_configuration(self.confs[i - 1])

        # initializing the vector of stored local energies and potential
        # energies
        self.elocs = np.zeros((2 * self.P + 1))
        self.elocs = self.local_energy(self.confs)

        self.pots = np.zeros((2 * self.P + 1))
        self.pots = self.potential(self.confs)

        self.num_accepted = 0
        self.num_moves = 0

    def local_energy(self, x):
        """
        Computes the local energy for an arbitrary potential
        """
        return self.psi.local_kinetic_energy(x) + self.potential(x)

    def get_new_configuration(self, x):
        """
        Returns a new configuration x' with the transition probabilities T(x->x')
        of the free particle propagator, starting from the configuration x.
        """
        return np.random.normal(x, self.SqrDt)

    def move(self):
        # pick a random direction
        direction = np.random.randint(2)

        if direction:
            # move left
            xprime = self.get_new_configuration(self.confs[0])

            potprime = self.potential(xprime)

            acceptance_ratio = -0.5 * self.Dt * (
                potprime + self.pots[0] - self.pots[-2] - self.pots[-1]
            )
            acceptance_ratio += self.psi.log_psi(xprime) - self.psi.log_psi(self.confs[0])
            acceptance_ratio += self.psi.log_psi(self.confs[-2]) - self.psi.log_psi(self.confs[-1])
            acceptance_ratio = np.exp(acceptance_ratio)
        else:
            # move right
            xprime = self.get_new_configuration(self.confs[-1])

            potprime = self.potential(xprime)

            acceptance_ratio = -0.5 * self.Dt * (
                potprime + self.pots[-1] - self.pots[1] - self.pots[0]
            )
            acceptance_ratio += self.psi.log_psi(xprime) - self.psi.log_psi(self.confs[-1])
            acceptance_ratio += self.psi.log_psi(self.confs[1]) - self.psi.log_psi(self.confs[0])
            acceptance_ratio = np.exp(acceptance_ratio)

        # Metropolis-Hastings acceptance_rate Test
        if acceptance_ratio > np.random.uniform():
            rolldir = 2 * direction - 1
            appos = direction - 1
            self.confs = np.roll(self.confs, rolldir)
            self.confs[appos] = xprime

            self.elocs = np.roll(self.elocs, rolldir)
            self.pots = np.roll(self.pots, rolldir)

            self.elocs[appos] = self.local_energy(xprime)
            self.pots[appos] = potprime

            self.num_accepted += 1.

        self.num_moves += 1.

    def acceptance_rate(self):
        return self.num_accepted / float(self.num_moves)

    def energy_estimator(self):
        return 0.5 * (self.elocs[0] + self.elocs[-1])

    def potential_energy_estimator(self):
        return self.pots[self.P]
