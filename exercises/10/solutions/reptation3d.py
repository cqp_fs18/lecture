#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Computation Quantum Physics, FS18

from __future__ import division, print_function

import abc

import numpy as np

from initial_states import GaussianInitialState

class Reptation3D:
    """
    This class implements the Reptation Quantum Monte Carlo for 3D particles.
    """
    def __init__(self, psi, tau, dtau, dim=3):

        # total imaginary time
        self.tau = tau

        # number of time slices
        self.P = int(tau / dtau)
        assert self.P >= 1

        # we recompute the time step to avoid truncation problems in the
        # division
        self.Dt = tau / float(self.P)
        self.SqrDt = np.sqrt(self.Dt)

        # path of configurations
        self.confs = np.zeros((2 * self.P + 1, psi.num_particles, dim))
        # avoid r1 == r2, and r1 == 0, r2 == 0
        self.confs[0] = np.random.random((psi.num_particles, dim))

        # setting the wave-function and the potential
        self.psi = psi

        # Initializing the path configurations with free propagation
        for i in range(1, len(self.confs)):
            self.confs[i] = self.get_new_configuration(self.confs[i - 1])

        # initializing the vector of stored local energies and potential
        # energies
        self.elocs = np.zeros((2 * self.P + 1))
        self.elocs = np.array([self.local_energy(x) for x in self.confs])

        self.num_accepted = 0
        self.num_moves = 0

    def local_energy(self, x):
        """
        Computes the local energy for an arbitrary potential
        """
        return self.psi.local_energy(x)

    def get_new_configuration(self, x):
        """
        Returns a new configuration x' with the transition probabilities T(x->x')
        of the free particle propagator, starting from the configuration x.
        """
        return np.random.normal(x + self.Dt * self.psi.grad_log_psi(x), self.SqrDt)

    def move(self):
        # pick a random direction
        direction = np.random.randint(2)

        if direction:
            # move left
            xprime = self.get_new_configuration(self.confs[0])

            elocprime = self.psi.local_energy(xprime)

            acceptance_ratio = -0.5 * self.Dt * (
                elocprime + self.elocs[0] - self.elocs[-2] - self.elocs[-1]
            )
            acceptance_ratio = np.exp(acceptance_ratio)
        else:
            # move right
            xprime = self.get_new_configuration(self.confs[-1])

            elocprime = self.psi.local_energy(xprime)

            acceptance_ratio = -0.5 * self.Dt * (
                elocprime + self.elocs[-1] - self.elocs[1] - self.elocs[0]
            )
            acceptance_ratio = np.exp(acceptance_ratio)

        # Metropolis-Hastings acceptance_rate Test
        if acceptance_ratio > np.random.uniform():
            rolldir = 2 * direction - 1
            appos = direction - 1
            self.confs = np.roll(self.confs, rolldir, axis=0)
            self.confs[appos] = xprime

            self.elocs = np.roll(self.elocs, rolldir)
            self.elocs[appos] = elocprime

            self.num_accepted += 1.

        self.num_moves += 1.

    def acceptance_rate(self):
        return self.num_accepted / float(self.num_moves)

    def energy_estimator(self):
        return 0.5 * (self.elocs[0] + self.elocs[-1])
