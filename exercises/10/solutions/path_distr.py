#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Computation Quantum Physics, FS18

import numpy as np
from matplotlib import pyplot as plt
from scipy.stats import gaussian_kde

from reptation import ReptationQMC
from initial_states import GaussianInitialState

def kde(x, x_grid, bandwidth=0.2, **kwargs):
    # Kernel Density Estimation with Scipy
    # This is a robust way to estimate probability distribution without using
    # histograms
    kde = gaussian_kde(x, bw_method=bandwidth, **kwargs)
    return kde.evaluate(x_grid)

def harmonic(x):
    return 0.5 * x**2.


psi = GaussianInitialState(-0.1)
dtau = 0.1

taus = [0.1, 0.5, 1, 5.0]

xv = np.linspace(-5, 5, 500, endpoint=True)

for tau in taus:
    qmc = ReptationQMC(psi, harmonic, tau, dtau)

    Nmoves = 400000
    confs = []
    for i in range(Nmoves):
        qmc.move()
        if(i > Nmoves / 3. and i % qmc.P == 0):
            confs.append(qmc.confs[qmc.P])

    print('tau=' + str(tau))
    # plt.hist(confs, 30, normed=1, alpha=0.15,label=r'$\tau ='+str(tau)+'$')
    plt.plot(xv, kde(confs, xv), label=r'$\tau =' + str(tau) + '$')

plt.plot(xv, np.exp(-xv**2.) / np.sqrt(np.pi), label='Exact', color='black')
plt.legend(frameon=False)
plt.ylabel(r'$\Psi(x,\tau)^2$')
plt.xlabel(r'$x$')
plt.show()
