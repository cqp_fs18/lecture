#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Computation Quantum Physics, FS18

import numpy as np
import scipy.linalg as la

class GaussianInitialState:
    num_particles = 1

    def __init__(self, alpha):
        self.alpha = alpha

    def log_psi(self, x):
        return self.alpha * x**2.

    def local_kinetic_energy(self, x):
        return -0.5 * (2. * self.alpha + (2. * x * self.alpha)**2)

class JastrowInitialState:
    num_particles = 2

    def __init__(self, p):
        self.p = p

    def log_psi(self, x):
        x = _CoordinatePair(x)
        return -2 * x.r1 - 2 * x.r2 + x.r12 / (2 * (1 + self.p * x.r12))

    def grad_log_psi(self, x):
        try:
            x = _CoordinatePair(x)
            a = (1 / x.r12 - self.p) * (x.r1_vec - x.r2_vec) / (2 * (1 + self.p * x.r12))
            return np.array([
                -2 * x.r1_vec / x.r1 + a,
                -2 * x.r2_vec / x.r2 - a
            ])
        except ZeroDivisionError:
            return 0

    def local_energy(self, x):
        x = _CoordinatePair(x)
        den = 1 / (1 + self.p * x.r12)
        den2 = den**2
        return (
            -4 + self.p * (den + den**2 + den * den2) - den2**2 / 4 +
            np.dot(x.r12_hat, x.r1_hat - x.r2_hat) * den2
        )

class _CoordinatePair:
    def __init__(self, x):
        self.r1_vec, self.r2_vec = x
        self.r1 = la.norm(self.r1_vec)
        self.r2 = la.norm(self.r2_vec)
        self.r12_vec = self.r1_vec - self.r2_vec
        self.r12 = la.norm(self.r12_vec)
        self.r12_hat = self.r12_vec / self.r12
        self.r1_hat = self.r1_vec / self.r1
        self.r2_hat = self.r2_vec / self.r2
