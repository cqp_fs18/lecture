#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Computation Quantum Physics, FS18

from __future__ import division, print_function

import numpy as np
import scipy.linalg as la
from matplotlib import pyplot as plt

from reptation3d import Reptation3D
from initial_states import JastrowInitialState

def autocorrelation(x):
    """
    Compute the autocorrelation of the signal, based on the properties of the
    power spectral density of the signal.
    """
    xp = x - np.mean(x)
    f = np.fft.fft(xp)
    p = np.array([np.real(v)**2 + np.imag(v)**2 for v in f])
    pi = np.fft.ifft(p)
    return np.real(pi)[:x.size // 2] / np.sum(xp**2)


def stats(x):
    ac = autocorrelation(x)
    taucorr = np.argmax(ac < 0)
    taucorr = 0.5 + 2. * np.sum(ac[0:taucorr])
    sigma = np.var(x) * taucorr / float(len(x))
    sigma = np.sqrt(sigma)
    return np.mean(x), sigma, taucorr

def main():

    psi = JastrowInitialState(0.5)
    dtau = 0.01

    taus = np.arange(dtau, 30 * dtau, 2 * dtau)
    eloctau = []
    eloctauerr = []
    for tau in taus:
        qmc = Reptation3D(psi, tau, dtau)

        Nmoves = 100000
        elocs = []
        for i in range(Nmoves):
            qmc.move()
            if(i > Nmoves / 3. and i % qmc.P == 0):
                elocs.append(qmc.energy_estimator())

        elocav, elocsig, taucorr = stats(np.asarray(elocs))
        eloctau.append(elocav)
        eloctauerr.append(elocsig)
        print(
            "tau = ", tau, " <H> = ", elocav, " +/- ", elocsig,
            " taucorr = ", taucorr, " acceptance_rate = ", qmc.acceptance_rate()
        )

    exact_energy = -2.90338583

    plt.errorbar(taus, eloctau, yerr=eloctauerr)
    plt.axhline(exact_energy, color='red', label='Exact')
    plt.xlabel(r'$\tau$')
    plt.ylabel(r'$\langle H(\tau) \rangle$')
    plt.legend(frameon=False)
    plt.show()

if __name__ == '__main__':
    main()
