#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Computation Quantum Physics, FS18

from __future__ import division, print_function

from matplotlib import pyplot as plt

from reptation import *
from initial_states import GaussianInitialState

def autocorrelation(x):
    """
    Compute the autocorrelation of the signal, based on the properties of the
    power spectral density of the signal.
    """
    xp = x - np.mean(x)
    f = np.fft.fft(xp)
    p = np.array([np.real(v)**2 + np.imag(v)**2 for v in f])
    pi = np.fft.ifft(p)
    return np.real(pi)[:x.size // 2] / np.sum(xp**2)


def stats(x):
    ac = autocorrelation(x)
    taucorr = np.argmax(ac < 0)
    taucorr = 0.5 + 2. * np.sum(ac[0:taucorr])
    sigma = np.var(x) * taucorr / float(len(x))
    sigma = np.sqrt(sigma)
    return np.mean(x), sigma, taucorr

def main(potential):

    psi = GaussianInitialState(-0.1)
    dtau = 0.1

    taus = np.arange(dtau, 3, 2 * dtau)
    eloctau = []
    eloctauerr = []
    for tau in taus:
        qmc = ReptationQMC(psi, potential, tau, dtau)

        Nmoves = 100000
        elocs = []
        for i in range(Nmoves):
            qmc.move()
            if(i > Nmoves / 3. and i % qmc.P == 0):
                elocs.append(qmc.energy_estimator())

        elocav, elocsig, taucorr = stats(np.asarray(elocs))
        eloctau.append(elocav)
        eloctauerr.append(elocsig)
        print(
            "tau = ", tau, " <H> = ", elocav, " +/- ", elocsig,
            " taucorr = ", taucorr, " acceptance_rate = ", qmc.acceptance_rate()
        )

    if(potential == harmonic):
        exact_energy = 0.5
    else:
        exact_energy = 0.602405

    plt.errorbar(taus, eloctau, yerr=eloctauerr)
    plt.axhline(exact_energy, color='red', label='Exact')
    plt.xlabel(r'$\tau$')
    plt.ylabel(r'$\langle H(\tau) \rangle$')
    plt.legend(frameon=False)
    plt.show()

if __name__ == '__main__':
    # Defining the potentials
    def harmonic(x):
        return 0.5 * x**2.

    def anharmonic(x):
        return 0.5 * x**2. + 0.2 * x**4.

    print('Harmonic potential:')
    main(potential=harmonic)

    print('\nAnharmonic potential:')
    main(potential=anharmonic)
