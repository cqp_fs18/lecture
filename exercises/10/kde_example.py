#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Computational Quantum Physics, FS18

import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import gaussian_kde

def kde(x, x_grid, bandwidth=0.2, **kwargs):
    # Kernel Density Estimation with Scipy
    # This is a robust way to estimate probability distribution without using
    # histograms
    kde = gaussian_kde(x, bw_method=bandwidth, **kwargs)
    return kde.evaluate(x_grid)

if __name__ == '__main__':
    values = [0, 1, 0.7, 0.8, 0.77, 0.69]
    x_grid = np.linspace(0, 1, 500)
    for bandwidth in [0.02, 0.05, 0.2, 1]:
        plt.plot(x_grid, kde(values, x_grid, bandwidth=bandwidth), label=str(bandwidth))
    plt.legend()
    plt.show()
