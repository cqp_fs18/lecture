# Computational Quantum Physics

Alexey A. Soluyanov, Guglielmo Mazzola

This is the repository for the Computational Quantum Physics lecture of the spring semester 2018. All course information, such as lecture slides, exercises and examples can be found here.

Class: HIL E7, 09:45-11:30  
Exercises: HIL E7, 12:45-14:30

Mailing list for questions: [cqp_fs18@lists.phys.ethz.ch](mailto:cqp_fs18@lists.phys.ethz.ch)

## Lecture slides

* Week  1: [Quantum Mechanics](https://gitlab.ethz.ch/cqp_fs18/lecture/raw/master/slides/Slides_Lecture_1_20.02.18.pdf)
* Week  2: [The Quantum One-Body Problem](https://gitlab.ethz.ch/cqp_fs18/lecture/raw/master/slides/Slides_Lecture_2_27.02.2018.pdf)
* Week  3: [Many-Body Schrödinger Equation](https://gitlab.ethz.ch/cqp_fs18/lecture/raw/master/slides/Slides_Lecture_3_6.03.2018.pdf)
* Week  4: [Exact Diagonalization of Spin Systems](https://gitlab.ethz.ch/cqp_fs18/lecture/raw/master/slides/Slides_Lecture_4_13.03.2018.pdf)
* Week  5: [Time Evolution of Spin Systems](https://gitlab.ethz.ch/cqp_fs18/lecture/raw/master/slides/Slides_Lecture_5_20.03.2018.pdf)
* Week  9: [Quantum Field Theory](https://gitlab.ethz.ch/cqp_fs18/lecture/raw/master/slides/Slides_Lecture_9_24.04.2018.pdf)
* Week 10: [Path Integral Monte Carlo](https://gitlab.ethz.ch/cqp_fs18/lecture/raw/master/script/cqp_10_pimc.pdf)
* Week 11: [Reptation Monte Carlo](https://gitlab.ethz.ch/cqp_fs18/lecture/raw/master/script/cqp_10_rqmc.pdf)
* Week 12: [Compressing the Wave Function](https://gitlab.ethz.ch/cqp_fs18/lecture/raw/master/script/cqp_14.pdf)

You can also go to an overview of the [lecture slides](slides/) and the [lecture notes](script/).

## Exercises

* Week  2: [Exercise 1](https://gitlab.ethz.ch/cqp_fs18/lecture/raw/master/exercises/01/exercise01.pdf), [Solution](https://gitlab.ethz.ch/cqp_fs18/lecture/raw/master/exercises/01/solutions/solution_01.zip)
* Week  3: [Exercise 2](https://gitlab.ethz.ch/cqp_fs18/lecture/raw/master/exercises/02/exercise02.pdf), [Solution](https://gitlab.ethz.ch/cqp_fs18/lecture/raw/master/exercises/02/solutions/solution_02.zip)
* Week  4: [Exercise 3](https://gitlab.ethz.ch/cqp_fs18/lecture/raw/master/exercises/03/exercise03.pdf), [Solution](https://gitlab.ethz.ch/cqp_fs18/lecture/raw/master/exercises/03/solutions/solutions_03.zip)
* Week  5: [Exercise 4](https://gitlab.ethz.ch/cqp_fs18/lecture/raw/master/exercises/04/exercise04.pdf), [Skeleton code](https://gitlab.ethz.ch/cqp_fs18/lecture/raw/master/exercises/04/tf_ising_skeleton.py), [Solution](https://gitlab.ethz.ch/cqp_fs18/lecture/raw/master/exercises/04/solutions/solutions_04.zip)
* Week  6: [Exercise 5](https://gitlab.ethz.ch/cqp_fs18/lecture/raw/master/exercises/05/exercise05.pdf)
* Week  7: [Exercise 6](https://gitlab.ethz.ch/cqp_fs18/lecture/raw/master/exercises/06/exercise06.pdf)
* Week  8: [Exercise 7](https://gitlab.ethz.ch/cqp_fs18/lecture/raw/master/exercises/07/exercise07.pdf), [Solution](https://gitlab.ethz.ch/cqp_fs18/lecture/raw/master/exercises/07/solutions/solutions_07.zip)
* Week  9: [Exercise 8](https://gitlab.ethz.ch/cqp_fs18/lecture/raw/master/exercises/08/exercise08.pdf), [Solution](https://gitlab.ethz.ch/cqp_fs18/lecture/raw/master/exercises/08/solution08.pdf)
* Week 10: [Exercise 9](https://gitlab.ethz.ch/cqp_fs18/lecture/raw/master/exercises/09/exercise09.pdf), [Solution](https://gitlab.ethz.ch/cqp_fs18/lecture/raw/master/exercises/09/solutions/solutions_09.zip)
* Week 11: [Exercise 10](https://gitlab.ethz.ch/cqp_fs18/lecture/raw/master/exercises/10/exercise10.pdf), [Skeleton codes](https://gitlab.ethz.ch/cqp_fs18/lecture/raw/master/exercises/10/skeleton_codes.zip), [KDE example](https://gitlab.ethz.ch/cqp_fs18/lecture/raw/master/exercises/10/kde_example.py), [Solution](https://gitlab.ethz.ch/cqp_fs18/lecture/raw/master/exercises/10/solutions_10.zip)
* Week 12: [Exercise 11](https://gitlab.ethz.ch/cqp_fs18/lecture/raw/master/exercises/11/exercise11.pdf), [Skeleton codes](https://gitlab.ethz.ch/cqp_fs18/lecture/raw/master/exercises/11/skeleton_codes.zip), [Solution](https://gitlab.ethz.ch/cqp_fs18/lecture/raw/master/exercises/11/solutions_11.zip)

**Hand-in**: Exercises should be handed in to the mailing list by **Sunday, 5 a.m.**, and will be corrected until Tuesday, 5 a.m. There are two ways to hand in your code:
* Give all TAs (@drodic, @greschd) **master** access to a git repository, and write an email to the mailing list when you are done with the exercise.
* Attach the code directly to the email you send to the mailing list.

## Additional material

Here you can find additional material, such as slides or examples from the exercise sessions.

* Week 1
    - [Python slides](https://gitlab.ethz.ch/cqp_fs17/lecture/raw/master/additional_material/01_python_intro.pdf)
    - [C++ and Makefile examples](additional_material/01_cpp)

## Exam Content

All content of the provided lecture slides, script and exercises is part of the exam, with the following exceptions:

* Week 3: Slides on "Applications of Tight-Binding" and "Geometrical Quantum Numbers" (pages 34ff.)
* Week 4: Lanczos Algorithm
* Weeks 6 / 7: Quantum Computing (whole subject)
* Week 8: Effective temperature (part of script section 10.3.2)
* Week 9: Gauge Theory (slides 39ff., script chapters 4 - 6)
* Week 10: Bose symmetry (script section 12.2.1), Moving multiple beads (section 12.3.2)
* Week 11: Diffusion QMC (script section 11.6)
* Week 12: Script appendices 14.A, 14.B
