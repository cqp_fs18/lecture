// header guard, prevents inlcuding twice
#ifndef ADD_HPP_GUARD
#define ADD_HPP_GUARD

// add is a function that accepts two ints and returns an int
int add(int a, int b) {
    return a + b;  // TA: return statement
}

#endif  // ADD_HPP_GUARD
