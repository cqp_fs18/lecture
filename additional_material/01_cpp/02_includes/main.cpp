// #include "": local include, relative
#include "add.hpp"

// #include <>: global include
#include <iostream>

int main() {
    // int is a type for integral numbers
    int a;
    int b;

    std::cout << "insert a number: ";
    // cin reads from the terminal, accepts after enter
    std::cin >> a;

    std::cout << "insert a second number: ";
    std::cin >> b;

    std::cout << "the sum is: " << add(a, b) << std::endl;

    return 0;
}
