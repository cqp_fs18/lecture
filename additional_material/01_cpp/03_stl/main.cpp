#include <cmath>  // for sqrt
#include <iostream>
#include <vector>

int main() {

    std::vector<double> vec;

    for(int i = 0; i < 10; ++i) {
        vec.push_back(std::sqrt(i));
    }

    for(uint i = 0; i < vec.size(); ++i) {
        std::cout << 2 * vec[i] << std::endl;
    }

    return 0;
}
