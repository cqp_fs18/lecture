// this let's us use std::cout
#include <iostream>

int main() {
    // std is the namespace
    // cout is the output to the terminal
    // << is the stream operator
    // "" is a string
    // endl means a new line
    // ; finishes the statement

    std::cout << "hello world!" << std::endl;

    // convention to return 0 if the program finishes with no error
    return 0;
}
