import matplotlib.pyplot as plt

with open('data.txt', 'r') as ins:
    data = tuple(ins)

plt.plot(data)
plt.savefig('plot.png')
