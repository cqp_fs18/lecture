# Simple Makefile example for Computational Quantum Physics.
# We focus on dependency chains and omit automatic variables and wildcards.

# Execute this file by calling 'make' on a terminal in this directory.

# Makefile rule syntax:
# targetname: dependency1 dependency2
# <tab>		body with instructions to execute

# Convention: the top target is named 'all' and used to select other targets 
.PHONY: all 
all: plot.png

# Show (and generate if necessary) the plot with the Ubuntu image viewer eog
# PHONY targets are non-files, not compared by file access time and will always execute the body
.PHONY: showplot
showplot: plot.png
	eog plot.png

# Generate the plot with a Python script, if the data is newer
plot.png: data.txt
	python plot.py

# Run our program and redirect the output into a data file, if this file is older (or nonexistent)
data.txt: test
	./test > data.txt

# Compile (or recompile) the program if the source code has changed
test: test.cpp add.hpp
	g++ test.cpp -o test -std=c++11

# IMPORTANT! Always provide a method to clean up after ourselves. Call with 'make clean'.
.PHONY: clean
clean:
	rm -f test
	rm -f data.txt
	rm -f plot.png
