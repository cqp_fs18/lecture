// header guard, prevents inlcuding twice
#ifndef ADD_HPP_GUARD
#define ADD_HPP_GUARD

// add is a function that accepts two doubles and returns a double
double add(double a, double b) {
    return a + b;  // return statement
}

#endif  // ADD_HPP_GUARD
