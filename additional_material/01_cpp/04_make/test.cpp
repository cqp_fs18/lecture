#include "add.hpp"

#include <cmath>  // for sqrt
#include <iostream>
#include <vector>

int main() {
    // <double> is a template argument

    // double is a floating point type

    std::vector<double> vec;

    for(int i = 0; i < 10; ++i) {
        vec.push_back(std::sqrt(i));
    }

    for(uint i = 0; i < vec.size(); ++i) {
        std::cout << add(vec[i], 1) << std::endl;
    }

    return 0;
}
